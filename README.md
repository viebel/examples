Example projects
===

A set of example projects that show how to small things in clojure and clojurescript.

[multi-select](https://gitlab.com/buildfunthings/examples/tree/master/multi-select): illustrate how to use re-frame to create a multi select widget. See [blog post 1](https://www.buildfunthings.com/a-multi-select-list-in-re-frame-and-bootstrap/) and [blog post 2](https://www.buildfunthings.com/a-multi-select-list-in-re-frame-part-2/).

[project-settings](https://gitlab.com/buildfunthings/examples/tree/master/project-settings): learn how to access leiningen project settings from clojurescript. See [this blog post](https://www.buildfunthings.com/accessing-project-settings-in-clojurescript/).
