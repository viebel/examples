(defproject project-settings "0.0.1-SNAPSHOT"
  :description "Access project settings in ClojureScript"
  :url "https://www.buildfunthings.com"

  :license {:name "GPLv3"
            :url "http://choosealicense.com/licenses/gpl-3.0/#"}

  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/clojurescript "1.9.293"]
                 [reagent "0.6.0"]]

  :plugins [[lein-cljsbuild "1.1.4"]]

  :profiles {:uberjar {:aot :all
                       :omit-source true
                       :prep-tasks ["compile" ["cljsbuild" "once" "min"]]}

             :dev  {:dependencies [[binaryage/devtools "0.8.3"]
                                   [com.cemerick/piggieback "0.2.1"]
                                   [figwheel-sidecar "0.5.8"]]

                    :plugins [[lein-figwheel "0.5.7"]
                              [lein-doo "0.1.7"]]}}

  :cljsbuild {:builds [{:id "dev"
                        :source-paths ["src/cljs"]
                        :figwheel {:on-jsload "project-settings.core/mount-root"}
                        :compiler {:main project-settings.core
                                   :output-to "resources/public/js/compiled/app.js"
                                   :output-dir "resources/public/js/compiled/out"
                                   :asset-path "js/compiled/out"
                                   :optimizations :simple
                                   :closure-defines {project-settings.core/project-version ~(System/getProperty "project-settings.version")}
                                   :source-map-timestamp true}}
                       ]})
